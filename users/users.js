
class Users {
    constructor() {

        this.getUser = (req, res) => {
            res.send(process.env.AWS_DEFAULT_REGION);
        }

        this.registerUser = (master, res, awsfunc, body) => {
            awsfunc.updateDynamoDb("company", body)
                .then(e => {
                    master.company.push(body);
                    res.json({ status: "Registered", id: body.id });
                })
                .catch(err => {
                    res.json(err)
                })
        }

        this.updateUser = (master, res, awsfunc, body) => {
            
            awsfunc.updateDynamoDb("company", body)
                .then(e => {
                    let checkCompany = master.company.filter(e => e.id === body.id)

                    if (checkCompany.length) {

                        for (var key in checkCompany[0]) {
                            if (checkCompany[0].hasOwnProperty(key)) {
                                checkCompany[0][key] = body[key]
                            }
                        }

                        res.json({ status: "Updated" });
                    }
                    else {
                        res.json({ status: "something wrong" });
                    }

                })
                .catch(err => {
                    res.json(err)
                })
        }

        this.deleteUser = (master, res, awsfunc, body) => {

            console.clear();
            console.log(body);

            let checkCompany = master.company.filter(e => e.id === body.id)

            if(checkCompany.length){

                if (checkCompany[0].logo === "") {
                    awsfunc.delItemsFromDynamo("company", checkCompany[0].id)
                        .then(e => {
    
                            master.company = master.company.filter(e => e.id != body.id)
                            res.json({ status: "Company Deleted" });
    
                        })
                        .catch(err => {
                            res.json(err)
                        })
                }
                else {
                    awsfunc.delObjS3(checkCompany[0].logo.replace("https://wow-cater-assets.s3.amazonaws.com/", "").replaceAll("%20", " "), "wow-cater-assets")
                        .then(e => {
                            awsfunc.delItemsFromDynamo("company", body.id)
                                .then(e => {
                                    master.company = master.company.filter(e => e.id != body.id)
                                    res.json({ status: "Company Deleted" });
    
                                })
                                .catch(err => {
                                    res.json(err)
                                })
                        })
                }

            }   
            else{
                res.json({ status: "Company not found" });
            }


            


        }


        this.uploadLogo = (master, res, awsfunc, body, file) => {

            let dir = "company/" + body.id + "/logo";


            let checkCompany = master.company.filter(e => e.id === body.id)

            if (checkCompany.length) {

                if (checkCompany[0].logo != "") {

                    awsfunc.delObjS3(checkCompany[0].logo.replace("https://wow-cater-assets.s3.amazonaws.com/", "").replaceAll("%20", " "), "wow-cater-assets")
                        .then(el => {
                            console.log(el);

                            awsfunc.s3Upload(dir, file.img, "wow-cater-assets")
                                .then(e => {
                                    console.log(e);
                                    checkCompany[0].logo = e.Location;

                                    awsfunc.updateDynamoDb("company", checkCompany[0])
                                        .then(el => {
                                            console.log(el);
                                            res.json({ status: "logo uploaded" });
                                        })
                                        .catch(err => {
                                            console.log(err);
                                            res.json(err)
                                        })
                                })
                                .catch(err => {
                                    res.json(err)
                                })
                        })
                        .catch(err => {
                            res.json(err)
                        })

                }
                else {

                    awsfunc.s3Upload(dir, file.img, "wow-cater-assets")
                        .then(e => {
                            console.log(e);
                            checkCompany[0].logo = e.Location;

                            awsfunc.updateDynamoDb("company", checkCompany[0])
                                .then(el => {
                                    console.log(el);
                                    res.json({ status: "logo uploaded" });
                                })
                                .catch(err => {
                                    console.log(err);
                                    res.json(err)
                                })
                        })
                        .catch(err => {
                            res.json(err)
                        })
                }
            }
            else {
                res.json({ status: "no entry found!" });
            }
        }

        this.companyProfile = (master,req,res,awsfunc) =>{
            let body = req.body
            let checkcompanys = master.company.filter(e => e.id === body.id)
            if(checkcompanys.length){
                let companyProfile = checkcompanys[0]
                delete companyProfile["pw"]
                res.json({status:"Company profile!",data:companyProfile})
            }else{
                res.json({status:"Company profile not found!"})
            }
        }

        this.placeOrder = (master,req,res,awsfunc, id) =>{
            console.clear();
            let body = req.body;
            if(!body.id){
                body.id = id;
            }   

            let checkMeal = master.admin.meals.filter(e => e.id === body.meal_id)
            if(checkMeal.length){
                let checkCompany = master.company.filter(e => e.id === body.company_id)
                if(checkCompany.length){
                    let checkVendor = master.company.filter(e => e.id === body.company_id)
                    if(checkVendor.length){
                        awsfunc.updateDynamoDb("orders", body)
                            .then(el =>{
                                master.admin.orders.push(body)
                                res.json({status:"Order placed!"})
                            })
                            .catch(err =>{
                                console.log(err);
                                res.json(err)
                            })
                    }else{
                        res.json({status:"Vendor not found!"})
                    }
                }else{
                    res.json({status:"Company not found!"})
                }
            }else{
                res.json({status:"Meal not found!"})
            }
        }

        this.orderList = (master, req, res, awsfunc) =>{
            console.clear();
            let body = req.body;

            let checkOrderList = master.admin.orders.filter(e => e.company_id === body.company_id)
            if(checkOrderList.length){
                res.json({status:"Order listed!",data:checkOrderList})
            }else{
                res.json({status:"Order not found!"})
            }
        }

        this.orderDetails = (master, req, res, awsfunc) =>{
            console.clear();
            let body = req.body;
            let checkOrder = master.admin.orders.filter(e => e.id === body.order_id && e.company_id === body.company_id)
            if(checkOrder.length){
                res.json({status:"Order found!",data : checkOrder[0]})
            }else{
                res.json({status:"Order not found!"})
            }
        }

    }



}

module.exports = Users;