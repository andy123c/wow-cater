let express = require("express");
let app  = express();
const router = express.Router();
var fs = require('fs');
var cors = require('cors');
require("dotenv").config();

const swaggerUi = require('swagger-ui-express');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerDef = require('./swaggerDef.js');

//-----swagger setup------
const swaggerOptions = {
	swaggerDefinition: swaggerDef,
	apis: ['./swagger/*.js']
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use('/api-doc', swaggerUi.serve, swaggerUi.setup(swaggerDocs));



const fileupload = require("express-fileupload");
app.use(fileupload());


//In order to send json data in post request
app.use(express.json());
//In order to send OBJ data in post request
app.use(express.urlencoded({extended:true}));


var jwt = require('jsonwebtoken');

var uniqid = require('uniqid'); 
require("dotenv").config();
var jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

//cors options
var corsOptions = {
    "origin": "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
    "preflightContinue": false,
    "optionsSuccessStatus": 200
}
app.use(cors(corsOptions));


//=====================================================================
//INIT
//=====================================================================

let users = require("./users/users")
let Users = new users();

let vendors = require("./vendor/vendor")
let Vendors = new vendors();

let admin = require("./admin/admin");
let Admin = new admin();

let awsfunc = require("./Aws");


let master = {
    company:[],
    vendors:[],
    admin:{
        menus:[],
        menutype:[],
        additionalMenus:[],
        orders:[],
        adminUsers:[],
        cuisines:[],
        meals:[],
        miscellaneous:[]
    }
}

//=====================================================================
//INIT
//=====================================================================


//public routes
app.get("/", (req,res)=>{
    res.send("wowcater backend")
    //Users.getUser(req,res)
})


let admnData = (e) => {

    fs.readFile("./admin.json", (err, data) => {
        if(err){
            console.log(err);            
        }
        else{
            master.admin.adminUsers = JSON.parse(data).users;
            obtained[e] = true
        }
    })    
}






//=====================================================================
//GET
//=====================================================================
// app.use('/api/v1', router);
//==========================
//all companies
app.get("/users", (req,res)=>{
    let company = jwt.sign({company:master.company}, "wowSecrets");
    res.json({company})
})

//==========================
//all vendors
app.get("/vendors", (req,res)=>{
    let vendors = jwt.sign({vendors:master.vendors}, "wowSecrets");
    res.json({vendors})
})

//==========================
//all menus
app.get("/menus", (req,res)=>{
    let menus = jwt.sign({menus:master.admin.menus}, "wowSecrets");
    res.json({menus})
})

//==========================
//all additionalMenus
app.get("/additional-menus", (req,res)=>{
    let additionalMenus = jwt.sign({menus:master.admin.additionalMenus}, "wowSecrets");
    res.json({additionalMenus})
})

//==========================
//all orders
app.get("/orders", (req,res)=>{
    let orders = jwt.sign({menus:master.admin.orders}, "wowSecrets");
    res.json({orders})
})
//==========================
//get miscellaneous
app.get("/miscellaneous", (req,res)=>{
    let miscellaneous = jwt.sign({menus:master.admin.miscellaneous}, "wowSecrets");
    res.json({miscellaneous})
})




app.get("/master", (req,res)=>{
    res.json({master})
})




























//=====================================================================
//POST
//=====================================================================






//==========================
//register a company
app.post("/user/register", (req,res)=>{

    console.clear();

    let body = req.body;
    const salt = bcrypt.genSaltSync();
    let password = bcrypt.hashSync(body["pw"], salt);

    body["id"] = uniqid()
    body["pw"] = password;

    let checkCompany = master.company.filter(e => e.email === body.email)

    if(checkCompany.length){
        res.json({status:"Another entry with same email exist"})
    }
    else{
        console.log(body);
        Users.registerUser(master,res,awsfunc, body)
    }

})

//==========================
//update a company
app.post("/user/update", (req,res)=>{

    console.clear();

    let body = req.body;

    console.log(body);

    let checkCompany = master.company.filter(e => e.id === body.id)

    if(checkCompany.length){
        Users.updateUser(master,res,awsfunc, body)        
    }
    else{
        res.json({status:"No company was found!"})        
    }

})

//==========================
//logs a company in
app.post("/user/login", (req,res)=>{
    
    let body = req.body;

    // let checkCompany = master.company.filter(e => e.email === body.email && e.pw === body.pw)
    let checkCompany = master.company.filter(e => e.email === body.email)

    if(checkCompany.length){
        if(bcrypt.compareSync(body.pw, checkCompany[0].pw)){
            res.json({status:"logged in!", id: checkCompany[0].id}) 
        }else{
            res.json({status:"Password does not exist!"})  
        }  
    }
    else{
        res.json({status:"Email does not exist!"})        
    }

})

//==========================
//deletes a company
app.post("/user/delete", (req,res)=>{

    console.clear();

    let body = req.body;
    
    let checkCompany = master.company.filter(e => e.id === body.id)

    if(checkCompany.length){
        Users.deleteUser(master,res,awsfunc, body)
    }
    else{
        res.json({status:"No company was found!"})        
    }

})

//==========================
//upload logo
app.post("/user/uploadlogo", (req,res) =>{

    console.clear();

    let body = req.body;
    let file = req.files;

    console.log(file);
    console.log(body.id);
    Users.uploadLogo(master,res,awsfunc, body, file)

})

//Company profile
app.post("/user/profile", (req,res) =>{
    Users.companyProfile(master,req,res,awsfunc)

})
// company order
app.post("/user/placeorder", (req,res) =>{
    Users.placeOrder(master,req,res,awsfunc, uniqid())
})
// company order list
app.post("/user/orderlist", (req,res) =>{
    Users.orderList(master,req,res,awsfunc)
})
// company order details
app.post("/user/orderdetails", (req,res) =>{
    Users.orderDetails(master,req,res,awsfunc)
})


//==========================//==========================//==========================


//==========================
//register a vendor
app.post("/vendor/register", (req,res)=>{

    console.clear();
    let body = req.body;
    const salt = bcrypt.genSaltSync();
    body["id"] = uniqid()
    body["pw"] = bcrypt.hashSync(body["pw"], salt)
    let checkVendor = master.vendors.filter(e => e.email === body.email)

    if(checkVendor.length){
        res.json({status:"Another entry with same email exist"})
    }
    else{
        Vendors.registerVendor(master,res,awsfunc, body)
    }

})

//==========================
//login a vendor
app.post("/vendor/login", (req,res)=>{

    let body = req.body;

    // let checkvendor = master.vendors.filter(e => e.email === body.email && e.pw === body.pw)
    let checkvendor = master.vendors.filter(e => e.email === body.email)

    if(checkvendor.length){
        if(bcrypt.compareSync(body.pw, checkvendor[0].pw)){
            res.json({status:"logged in!", id: checkvendor[0].id})
        }else{
            res.json({status:"Password does not exist!"})     
        }              
    }
    else{
        res.json({status:"Email does not exist!"})       
    }

})

//==========================
//delete a vendor
app.post("/vendor/delete", (req,res)=>{

    console.clear();

    let body = req.body;
    
    let checkVendor = master.vendors.filter(e => e.id === body.id)

    if(checkVendor.length){
        Vendors.deleteVendor(master,res,awsfunc, body)
    }
    else{
        res.json({status:"No company was found!"})        
    }

})

//==========================
//update a vendor
app.post("/vendor/update", (req,res)=>{
    console.clear();
    let body = req.body;    
    let checkVendor = master.vendors.filter(e => e.id === body.id)

    if(checkVendor.length){
        Vendors.updateVendor(master,res,awsfunc, body)
    }
    else{
        res.json({status:"No company was found!"})        
    }
})

//==========================
//upload vendor logo
app.post("/vendor/uploadlogo", (req,res)=>{
    console.clear();

    let body = req.body;
    let file = req.files;

    console.log(file);
    console.log(body.id);
    Vendors.uploadLogo(master,res,awsfunc, body, file)
})

//==========================
//upload vendor menu
app.post("/vendor/uploadmenu", (req,res)=>{
    console.clear();
   

    if(req.files){

        let body = req.body;
        body["id"] = uniqid();

        let file = req.files;
        Vendors.uploadmenu(master,res,awsfunc, body, file)
    }
    else{
        res.json({status:"no image was selected"})
    }
    
})

app.post("/vendor/updatemenu", (req,res) =>{

    Vendors.updateMenu(master,req, res,awsfunc, uniqid())

})

app.post("/vendor/createmenuList", (req,res) =>{

    Vendors.createMenuList(master,req, res,awsfunc, uniqid())

})

app.post("/vendor/deletemenuList", (req,res) =>{

    Vendors.deleteMenuList(master, req,res,awsfunc)

})


app.post("/vendor/editmenuList", (req,res) =>{

    Vendors.editmenuList(master,req,res,awsfunc)

})
//==========================
//Vendor profile
app.post("/vendor/profile", (req,res) =>{
    Vendors.vendorProfile(master,req,res,awsfunc)

})
//==========================
//Vendor add meal
app.post("/vendor/uploadmeal", (req,res)=>{
    console.clear();
    Vendors.addMeal(master, req,res, awsfunc, uniqid())
})
//==========================
//Vendor edit meal
app.post("/vendor/editmeal", (req,res)=>{
    console.clear();
    Vendors.editMeal(master, req,res, awsfunc)
})
//==========================
//Vendor update meal
app.post("/vendor/updatemeal", (req,res)=>{
    console.clear();
    Vendors.updateMeal(master, req,res, awsfunc)
})
//==========================
//Vendor delete meal
app.post("/vendor/deletemeal", (req,res)=>{
    console.clear();
    Vendors.deleteMeal(master, req,res, awsfunc)
})
//Vendor order list
app.post("/vendor/orderlist", (req,res) =>{
    console.clear();
    Vendors.orderList(master,req,res,awsfunc)
})
//Vendor order details
app.post("/vendor/orderdetails", (req,res) =>{
    console.clear();
    Vendors.orderDetails(master,req,res,awsfunc)
})

//==========================//==========================//==========================

//==========================
//login an admin
app.post("/admin/login", (req,res)=>{
    Admin.login(master, req,res)
})

app.post("/admin/uploadcuisines", (req,res)=>{
    console.clear();
    Admin.addCuisines(master, req,res, awsfunc, uniqid())
})

app.post("/admin/updatecuisines", (req,res)=>{

    Admin.updateCuisines(master, req,res, awsfunc)
})

app.post("/admin/deletecuisines", (req,res)=>{

    Admin.deleteCuisine(master, req,res, awsfunc)
})


app.post("/admin/uploadmenutype", (req,res)=>{
    console.clear();
    Admin.addMenutype(master, req,res, awsfunc, uniqid())
})

app.post("/admin/updatemenutype", (req,res)=>{
    console.clear();
    Admin.updatemenutype(master, req,res, awsfunc, uniqid())
})

app.post("/admin/delmenutype", (req,res)=>{
    Admin.deletemenutype(master, req,res, awsfunc)
})

app.post("/admin/uploadordertype", (req,res)=>{
    console.clear();
    Admin.addordertype(master, req,res, awsfunc, uniqid())
})
// add miscellaneous
app.post("/admin/uploadmiscellaneous", (req,res)=>{
    console.clear();
    Admin.addMiscellaneous(master, req,res, awsfunc, uniqid())
})
//update miscellaneous
app.post("/admin/updatemiscellaneous", (req,res)=>{
    // console.clear();
    Admin.updateMiscellaneous(master, req,res, awsfunc)
})












// app.post("/user/upload", (req,res)=>{

//     console.clear();

//     let file = req.files;

//     awsfunc.s3Upload("company/123456/logo", file.img, "wow-cater-assets")
//     .then(e =>{
//         console.log(e);
//     })
//     //company/123456/logo/db-scema.png

// })



// app.get("/user/upload-delete", (req,res)=>{

//     console.clear();

//     awsfunc.delObjS3("company/123456/logo/db-scema.png", "wow-cater-assets")
//     .then(e =>{
//         console.log(e);
//     })

//     //company/123456/logo/db-scema.png
    

// })

// app.get("/user/:userid/:empId", (req,res)=>{
//     //Users.login(req,res)
//     var userid = req.params.userid;
//     var empId = req.params.empId;
//     res.json({
//         userid:userid,
//         empId:empId
//     })
// })

// app.get("/user/:userid/order", (req,res)=>{
//     //Users.login(req,res)
//     var userid = req.params.userid;
//     res.json({
//         userid:userid,
//     })
// })



//==========================
//logs a company in




//=====================================================================
//calling all datas at the starting of the server
//=====================================================================
let obtained = {
    company:false,
    menu:false,
    vendors:false,
    additionalMenu:false,
    adminUsers:false,
    menutype:false,
    orders:false,
    meals:false,
    miscellaneous:false
}

let masterData = () =>{

    let callData = async () =>{
        awsfunc.getDynamoData("company")
        .then(data =>{
            //console.log(data.Items);
            master.company = data.Items
            obtained.company = true
        })
        .catch(err =>{
            console.log(err);
        })

        awsfunc.getDynamoData("menu")
        .then(data =>{
            master.admin.menu = []
            //console.log(data.Items);
            master.admin.menus = data.Items;
            obtained.menu = true;
        })
        .catch(err =>{
            console.log(err);
        })

        awsfunc.getDynamoData("orders")
        .then(data =>{
            master.admin.orders = []
            //console.log(data.Items);
            master.admin.orders = data.Items;
            obtained.orders = true;
        })
        .catch(err =>{
            console.log(err);
        })

        
        awsfunc.getDynamoData("vendors")
        .then(data =>{
            //console.log(data.Items);
            master.vendors = data.Items
            obtained.vendors = true;
        })
        .catch(err =>{
            console.log(err);
        })

        awsfunc.getDynamoData("additionalMenu")
        .then(data =>{
            //console.log(data.Items);
            master.additionalMenu = data.Items
            obtained.additionalMenu = true;
        })
        .catch(err =>{
            console.log(err);
        })

        awsfunc.getDynamoData("menutype")
        .then(data =>{
            //console.log(data.Items);
            master.admin.menutype = data.Items
            obtained.menutype = true;
        })
        .catch(err =>{
            console.log(err);
        })

        awsfunc.getDynamoData("cuisines")
        .then(data =>{
            //console.log(data.Items);
            master.admin.cuisines = data.Items
            obtained.cuisines = true;
        })
        .catch(err =>{
            console.log(err);
        })

        awsfunc.getDynamoData("meals")
        .then(data =>{
            //console.log(data.Items);
            master.admin.meals = data.Items
            obtained.meals = true;
        })
        .catch(err =>{
            console.log(err);
        })

        awsfunc.getDynamoData("miscellaneous")
        .then(data =>{
            //console.log(data.Items);
            master.admin.miscellaneous = data.Items
            obtained.miscellaneous = true;
        })
        .catch(err =>{
            console.log(err);
        })


        

        

        admnData("adminUsers");

        return await new Promise((resolve, reject) => {
            
            if(!obtained.company || !obtained.menu ||!obtained.vendors ||!obtained.additionalMenu || !obtained.adminUsers || !obtained.menutype || !obtained.orders || !obtained.cuisines || !obtained.meals || !obtained.miscellaneous ){
                reject("need to call again!");
            }
            else{
                resolve(master)
            }
        });
    }

    callData()
    .then(e =>{
        console.log("data!");
    })
    .catch(err => {
        console.log(err);
    })
}

masterData();

//================================================
//calling all datas at the starting of the server
//================================================

app.listen(9000)