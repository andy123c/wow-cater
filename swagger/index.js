//--------------- All Users List ---------------------------//
    /**
     * @swagger
     * components:
     *  schemas:
     *    allUsersList:

     */

    /**
     * @swagger
     * /users:
     *  get:
     *    tags:
     *    - Master Api
     *    summary: This api shows the all users list.
     *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
     *    operationId: allUsersList

     *    responses:
     *      200:
     *        description: successful response
     *      405:
     *        description: Invalid input
     */


//--------------- All Vendors List ---------------------------//

    /**
     * @swagger
     * components:
     *  schemas:
     *    allVendorList:

     */

    /**
     * @swagger
     * /vendors:
     *  get:
     *    tags:
     *    - Master Api
     *    summary: This api shows the all vendor list.
     *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
     *    operationId: allVendorList

     *    responses:
     *      200:
     *        description: successful response
     *      405:
     *        description: Invalid input
     */

//--------------- All Menus List ---------------------------//

    /**
     * @swagger
     * components:
     *  schemas:
     *    allMenuList:

     */

    /**
     * @swagger
     * /menus:
     *  get:
     *    tags:
     *    - Master Api
     *    summary: This api shows the all menu list.
     *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
     *    operationId: allMenuList

     *    responses:
     *      200:
     *        description: successful response
     *      405:
     *        description: Invalid input
     */

//--------------- All Additional Menus List ---------------------------//

    /**
     * @swagger
     * components:
     *  schemas:
     *    allAdditionalMenuList:

     */

    /**
     * @swagger
     * /additional-menus:
     *  get:
     *    tags:
     *    - Master Api
     *    summary: This api shows the all additional menu list.
     *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
     *    operationId: allAdditionalMenuList

     *    responses:
     *      200:
     *        description: successful response
     *      405:
     *        description: Invalid input
     */

//--------------- All orders List ---------------------------//

    /**
     * @swagger
     * components:
     *  schemas:
     *    allOrderList:

     */

    /**
     * @swagger
     * /orders:
     *  get:
     *    tags:
     *    - Master Api
     *    summary: This api shows the all orders list.
     *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
     *    operationId: allOrderList

     *    responses:
     *      200:
     *        description: successful response
     *      405:
     *        description: Invalid input
     */

//--------------- All miscellaneous List ---------------------------//

    /**
     * @swagger
     * components:
     *  schemas:
     *    allMiscellaneousList:

     */

    /**
     * @swagger
     * /miscellaneous:
     *  get:
     *    tags:
     *    - Master Api
     *    summary: This api shows the miscellaneous.
     *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
     *    operationId: allMiscellaneousList

     *    responses:
     *      200:
     *        description: successful response
     *      405:
     *        description: Invalid input
     */