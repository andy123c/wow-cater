//--------------- Vendor Registration ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    vendorRegister:
 *      required:
 *      - email
 *      - pw
 *      type: object
 *      properties:
 *        email:
 *          type: string
 *          example: "example@example.com"
 *        pw:
 *          type: string
 */

/**
 * @swagger
 * /vendor/register:
 *  post:
 *    tags:
 *    - Vendors
 *    summary: This api register new vendor.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: vendorRegister
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/vendorRegister'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Vendor Login ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    vendorLogin:
 *      required:
 *      - email
 *      - pw
 *      type: object
 *      properties:
 *        email:
 *          type: string
 *          example: "example@example.com"
 *        pw:
 *          type: string
 */

/**
 * @swagger
 * /vendor/login:
 *  post:
 *    tags:
 *    - Vendors
 *    summary: This api login a vendor.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: vendorLogin
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/vendorLogin'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Vendor Update ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    vendorUpdate:
 *      required:
 *      - id
 *      type: object
 *      properties:
 *        email:
 *          type: string
 */

/**
 * @swagger
 * /vendor/update:
 *  post:
 *    tags:
 *    - Vendors
 *    summary: This api update a vendor.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: vendorUpdate
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/vendorUpdate'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Vendor Delete ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    vendorDelete:
 *      required:
 *      - id
 *      type: object
 *      properties:
 *        id:
 *          type: string
 */

/**
 * @swagger
 * /vendor/delete:
 *  post:
 *    tags:
 *    - Vendors
 *    summary: This api delete a vendor.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: vendorDelete
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/vendorDelete'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */


//--------------- Vendor Upldate Logo  ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    vendorUploadlogo:
 *      required:
 *      - email
 *      - pw
 *      type: object
 *      properties:
 *        email:
 *          type: string
 *          example: "example@example.com"
 *        pw:
 *          type: string
 */

/**
 * @swagger
 * /vendor/uploadlogo:
 *  post:
 *    tags:
 *    - Vendors
 *    summary: This api vendor a upload logo.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: vendorUploadlogo
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/vendorUploadlogo'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Vendor Upload Menu  ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    vendorUploadMenu:
 *      required:
 *      - email
 *      - pw
 *      type: object
 *      properties:
 *        email:
 *          type: string
 *          example: "example@example.com"
 *        pw:
 *          type: string
 */

/**
 * @swagger
 * /vendor/uploadmenu:
 *  post:
 *    tags:
 *    - Vendors
 *    summary: This api vendor upload menu.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: vendorUploadMenu
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/vendorUploadMenu'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Vendor Update Menu  ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    vendorUpdateMenu:
 *      required:
 *      - email
 *      - pw
 *      type: object
 *      properties:
 *        email:
 *          type: string
 *          example: "example@example.com"
 *        pw:
 *          type: string
 */

/**
 * @swagger
 * /vendor/updatemenu:
 *  post:
 *    tags:
 *    - Vendors
 *    summary: This api vendor update a menu.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: vendorUpdateMenu
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/vendorUpdateMenu'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Vendor Crate Menu List ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    vendorCreateMenuList:
 *      required:
 *      - email
 *      - pw
 *      type: object
 *      properties:
 *        email:
 *          type: string
 *          example: "example@example.com"
 *        pw:
 *          type: string
 */

/**
 * @swagger
 * /vendor/createmenuList:
 *  post:
 *    tags:
 *    - Vendors
 *    summary: This api vendor create menu list.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: vendorCreateMenuList
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/vendorCreateMenuList'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Vendor Delete Menu List ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    vendorDeleteMenuList:
 *      required:
 *      - email
 *      - pw
 *      type: object
 *      properties:
 *        email:
 *          type: string
 *          example: "example@example.com"
 *        pw:
 *          type: string
 */

/**
 * @swagger
 * /vendor/deletemenuList:
 *  post:
 *    tags:
 *    - Vendors
 *    summary: This api vendor delete a menu list.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: vendorDeleteMenuList
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/vendorDeleteMenuList'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Vendor Edit Menu List ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    vendorEditMenuList:
 *      required:
 *      - email
 *      - pw
 *      type: object
 *      properties:
 *        email:
 *          type: string
 *          example: "example@example.com"
 *        pw:
 *          type: string
 */

/**
 * @swagger
 * /vendor/editmenuList:
 *  post:
 *    tags:
 *    - Vendors
 *    summary: This api vendor edit a menu list.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: vendorEditMenuList
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/vendorEditMenuList'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Vendor Profile ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    vendorProfile:
 *      required:
 *      - id
 *      type: object
 *      properties:
 *        id:
 *          type: string
 */

/**
 * @swagger
 * /vendor/profile:
 *  post:
 *    tags:
 *    - Vendors
 *    summary: This api vendor get his profile.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: vendorProfile
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/vendorProfile'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Vendor Add Meal ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    vendorAddMeal:
 *      required:
 *      - title
 *      - menu_id
 *      - vendor_price
 *      - price
 *      - protein
 *      - ingredients
 *      - dietary_information
 *      - description
 *      type: object
 *      properties:
 *        title:
 *          type: string
 *        menu_id:
 *          type: string
 *        vendor_price:
 *          type: integer
 *          format: int64
 *        price:
 *          type: integer
 *          format: int64
 *        protein:
 *          type: string
 *        ingredients:
 *          type: string
 *        dietary_information:
 *          type: string
 *        description:
 *          type: string
 */

/**
 * @swagger
 * /vendor/uploadmeal:
 *  post:
 *    tags:
 *    - Vendors
 *    summary: This api vendor add meal.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: vendorAddMeal
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/vendorAddMeal'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Vendor Edit Meal ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    vendorEditMeal:
 *      required:
 *      - id
 *      type: object
 *      properties:
 *        id:
 *          type: string
 */

/**
 * @swagger
 * /vendor/editmeal:
 *  post:
 *    tags:
 *    - Vendors
 *    summary: This api vendor edit a meal.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: vendorEditMeal
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/vendorEditMeal'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Vendor Update Meal ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    vendorUpdateMeal:
 *      required:
 *      - id
 *      - title
 *      - menu_id
 *      - vendor_price
 *      - price
 *      - protein
 *      - ingredients
 *      - dietary_information
 *      - description
 *      type: object
 *      properties:
 *        id:
 *          type: string
 *        title:
 *          type: string
 *        menu_id:
 *          type: string
 *        vendor_price:
 *          type: integer
 *          format: int64
 *        price:
 *          type: integer
 *          format: int64
 *        protein:
 *          type: string
 *        ingredients:
 *          type: string
 *        dietary_information:
 *          type: string
 *        description:
 *          type: string
 */

/**
 * @swagger
 * /vendor/updatemeal:
 *  post:
 *    tags:
 *    - Vendors
 *    summary: This api vendor update a meal.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: vendorUpdateMeal
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/vendorUpdateMeal'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Vendor Delete Meal ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    vendorDeleteMeal:
 *      required:
 *      - id
 *      type: object
 *      properties:
 *        id:
 *          type: string
 */

/**
 * @swagger
 * /vendor/deletemeal:
 *  post:
 *    tags:
 *    - Vendors
 *    summary: This api vendor delete a meal.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: vendorDeleteMeal
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/vendorDeleteMeal'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */


//--------------- Vendor Order List ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    vendorOrderList:
 *      required:
 *      - vendor_id
 *      type: object
 *      properties:
 *        vendor_id:
 *          type: string
 */

/**
 * @swagger
 * /vendor/orderlist:
 *  post:
 *    tags:
 *    - Vendors
 *    summary: This api vendor his all orders.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: vendorOrderList
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/vendorOrderList'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Vendor Order Details ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    vendorOrderDetails:
 *      required:
 *      - order_id
 *      - vendor_id
 *      type: object
 *      properties:
 *        order_id:
 *          type: string
 *        vendor_id:
 *          type: string
 */

/**
 * @swagger
 * /vendor/orderdetails:
 *  post:
 *    tags:
 *    - Vendors
 *    summary: This api vendor his all orders.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: vendorOrderDetails
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/vendorOrderDetails'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */