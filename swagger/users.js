
//--------------- User Registration ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    userRegister:
 *      required:
 *      - email
 *      - pw
 *      type: object
 *      properties:
 *        email:
 *          type: string
 *          example: "example@example.com"
*        pw:
 *          type: string
 */

/**
 * @swagger
 * /user/register:
 *  post:
 *    tags:
 *    - Users
 *    summary: This api register new user.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: userRegister
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/userRegister'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- User Update ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    userUpdate:
 *      required:
 *      - email
 *      - pw
 *      type: object
 *      properties:
 *        email:
 *          type: string
 *          example: "example@example.com"
*        pw:
 *          type: string
 */

/**
 * @swagger
 * /user/update:
 *  post:
 *    tags:
 *    - Users
 *    summary: This api update a user.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: userUpdate
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/userUpdate'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */


//--------------- User Login ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    userLogin:
 *      required:
 *      - email
 *      - pw
 *      type: object
 *      properties:
 *        email:
 *          type: string
 *          example: "example@example.com"
*        pw:
 *          type: string
 */

/**
 * @swagger
 * /user/login:
 *  post:
 *    tags:
 *    - Users
 *    summary: This api login a user.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: userLogin
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/userLogin'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- User Delete ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    userDelete:
 *      required:
 *      - id
 *      type: object
 *      properties:
 *        id:
 *          type: string
 */

/**
 * @swagger
 * /user/delete:
 *  post:
 *    tags:
 *    - Users
 *    summary: This api delete a user.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: userDelete
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/userDelete'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- User Upldate Logo ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    useruploadlogo:
 *      required:
 *      - email
 *      - pw
 *      type: object
 *      properties:
 *        email:
 *          type: string
 *          example: "example@example.com"
*        pw:
 *          type: string
 */

/**
 * @swagger
 * /user/uploadlogo:
 *  post:
 *    tags:
 *    - Users
 *    summary: This api user a upload logo.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: useruploadlogo
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/useruploadlogo'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- User Profile ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    userProfile:
 *      required:
 *      - id
 *      type: object
 *      properties:
 *        id:
 *          type: string
 */

/**
 * @swagger
 * /user/profile:
 *  post:
 *    tags:
 *    - Users
 *    summary: This api user get his profile.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: userProfile
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/userProfile'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- User Orders List ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    userOrdersList:
 *      required:
 *      - company_id
 *      type: object
 *      properties:
 *        company_id:
 *          type: string
 */

/**
 * @swagger
 * /user/orderlist:
 *  post:
 *    tags:
 *    - Users
 *    summary: This api vendor his all orders.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: userOrdersList
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/userOrdersList'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- User Orders Details ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    userOrderDetails:
 *      required:
 *      - order_id
 *      - company_id
 *      type: object
 *      properties:
 *        order_id:
 *          type: string
 *        company_id:
 *          type: string
 */

/**
 * @swagger
 * /user/orderdetails:
 *  post:
 *    tags:
 *    - Users
 *    summary: This api vendor his order details.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: userOrderDetails
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/userOrderDetails'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */