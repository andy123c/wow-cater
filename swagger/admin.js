//--------------- Admin Registration ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    adminRegister:
 *      required:
 *      - email
 *      - pw
 *      type: object
 *      properties:
 *        email:
 *          type: string
 *          example: "example@example.com"
 *        pw:
 *          type: string
 */

/**
 * @swagger
 * /admin/login:
 *  post:
 *    tags:
 *    - Admin
 *    summary: This api login a admin.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: adminRegister
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/adminRegister'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Admin Upload Cuisines ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    adminUploadcuisines:
 *      required:
 *      - email
 *      - pw
 *      type: object
 *      properties:
 *        email:
 *          type: string
 *          example: "example@example.com"
 *        pw:
 *          type: string
 */

/**
 * @swagger
 * /admin/uploadcuisines:
 *  post:
 *    tags:
 *    - Admin
 *    summary: This api login a admin.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: adminUploadcuisines
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/adminUploadcuisines'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Admin Update Cuisines ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    adminDeletecuisines:
 *      required:
 *      - email
 *      - pw
 *      type: object
 *      properties:
 *        email:
 *          type: string
 *          example: "example@example.com"
 *        pw:
 *          type: string
 */

/**
 * @swagger
 * /admin/deletecuisines:
 *  post:
 *    tags:
 *    - Admin
 *    summary: This api login a admin.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: adminDeletecuisines
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/adminDeletecuisines'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Admin Upload Menutype ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    adminUploadMenutype:
 *      required:
 *      - email
 *      - pw
 *      type: object
 *      properties:
 *        email:
 *          type: string
 *          example: "example@example.com"
 *        pw:
 *          type: string
 */

/**
 * @swagger
 * /admin/uploadmenutype:
 *  post:
 *    tags:
 *    - Admin
 *    summary: This api login a admin.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: adminUploadMenutype
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/adminUploadMenutype'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Admin Update Menutype ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    adminUpdateMenutype:
 *      required:
 *      - email
 *      - pw
 *      type: object
 *      properties:
 *        email:
 *          type: string
 *          example: "example@example.com"
 *        pw:
 *          type: string
 */

/**
 * @swagger
 * /admin/updatemenutype:
 *  post:
 *    tags:
 *    - Admin
 *    summary: This api login a admin.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: adminUpdateMenutype
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/adminUpdateMenutype'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Admin Delete Menutype ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    adminDelMenutype:
 *      required:
 *      - email
 *      - pw
 *      type: object
 *      properties:
 *        email:
 *          type: string
 *          example: "example@example.com"
 *        pw:
 *          type: string
 */

/**
 * @swagger
 * /admin/delmenutype:
 *  post:
 *    tags:
 *    - Admin
 *    summary: This api login a admin.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: adminDelMenutype
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/adminDelMenutype'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Admin Upload Ordertype ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    adminUploadOrdertype:
 *      required:
 *      - email
 *      - pw
 *      type: object
 *      properties:
 *        email:
 *          type: string
 *          example: "example@example.com"
 *        pw:
 *          type: string
 */

/**
 * @swagger
 * /admin/uploadordertype:
 *  post:
 *    tags:
 *    - Admin
 *    summary: This api login a admin.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: adminUploadOrdertype
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/adminUploadOrdertype'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */

//--------------- Admin Update miscellaneous ---------------------------//

/**
 * @swagger
 * components:
 *  schemas:
 *    adminUploadmiScellaneous:
 *      required:
 *      - id
 *      - commission
 *      - delivery_fee
 *      - get_swift_credentials
 *      type: object
 *      properties:
 *        id:
 *          type: string
 *        commission:
 *          type: integer
 *          format: int64
 *        delivery_fee:
 *          type: integer
 *          format: int64
 *        get_swift_credentials:
 *          type: string
 */

/**
 * @swagger
 * /admin/updatemiscellaneous:
 *  post:
 *    tags:
 *    - Admin
 *    summary: This api login a admin.
 *    description: "[Click here to see Work area screenshot](https://monosnap.com/file/jY5WwDUhXY4QtVetsJj5MHUW4IWhE9)"
 *    operationId: adminUploadmiScellaneous
 *    requestBody:
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/adminUploadmiScellaneous'
 *      required: true
 *    responses:
 *      200:
 *        description: successful response
 *      405:
 *        description: Invalid input
 */