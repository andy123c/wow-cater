class Admin {

    constructor() {

        this.login = (master, req, res) => {

            console.clear();

            let body = req.body;

            console.log(body)

            let adminUser = master.admin.adminUsers.filter(e => e.id === body.id && e.pw === body.pw)

            console.log(master.admin);

            if (adminUser.length) {
                res.json({ status: "login" })
            }
            else {
                res.json({ status: "wrong credentials" })
            }
        }

        //==========================Cuisins==============================

        this.addCuisines = (master, req, res, awsfunc, id) => {

            console.clear();

            let body = req.body;
            
            if(!body.id){
                body.id = id;
            }            

            if(req.files){
                let img = req.files.img;
                let dir = "cuisines/" + id

                awsfunc.s3Upload(dir, img, "wow-cater-assets")
                .then(e => {
                    console.log(e);

                    body.img = e.Location

                    awsfunc.updateDynamoDb("cuisines", body)
                    .then(el =>{
                        master.admin.cuisines.push(body)
                        res.json({status:"cuisine uploaded"});
                    })
                    .catch(err =>{
                        console.log(err);
                        res.json(err)
                    })
                })
            }
            else{
                awsfunc.updateDynamoDb("cuisines", body)
                .then(el =>{
                    master.admin.cuisines.push(body)
                    res.json({status:"cuisine uploaded"});
                })
                .catch(err =>{
                    console.log(err);
                    res.json(err)
                })
            }          

        }

        this.updateCuisines = (master, req, res, awsfunc) =>{

            console.clear();

            let body = req.body;

            let checkCuisine = master.admin.cuisines.filter(el => el.id === body.id)

            if(checkCuisine.length){

                if(req.files){

                    let img = req.files.img;
                    let dir = "cuisines/" + body.id

                    console.log(img);
    
                    if(checkCuisine[0].img === ""){

                        awsfunc.s3Upload(dir, img, "wow-cater-assets")
                        .then(e => {
                            console.log(e);

                            body.img = e.Location

                            awsfunc.updateDynamoDb("cuisines", body)
                            .then(el =>{
                                //master.admin.cuisines.push(body)

                                for (var key in checkCuisine[0]) {
                                    if (checkCuisine[0].hasOwnProperty(key)) {
                                        checkCuisine[0][key] = body[key]
                                    }
                                }

                                res.json({status:"cuisine updated"});
                            })
                            .catch(err =>{
                                console.log(err);
                                res.json(err)
                            })
                        })    
                        
                    }
                    else{

                        // {
                        //     ETag: '"33725366302324375833b2ceed96873c"',
                        //     Location: 'https://wow-cater-assets.s3.amazonaws.com/cuisines/8sb517t2l58j83p7/Python.png',
                        //     key: 'cuisines/8sb517t2l58j83p7/Python.png',
                        //     Key: 'cuisines/8sb517t2l58j83p7/Python.png',
                        //     Bucket: 'wow-cater-assets'
                        //   }

                        // {
                        //     ETag: '"ac2c7ea04d9d418b750845e625c2df0e"',
                        //     Location: 'https://wow-cater-assets.s3.amazonaws.com/cuisines/8sb517t2l58j83p7/Java%20Script.png',
                        //     key: 'cuisines/8sb517t2l58j83p7/Java Script.png',
                        //     Key: 'cuisines/8sb517t2l58j83p7/Java Script.png',
                        //     Bucket: 'wow-cater-assets'
                        //   }                         
                          

                        awsfunc.delObjS3(checkCuisine[0].img.replace("https://wow-cater-assets.s3.amazonaws.com/", "").replaceAll("%20", " "), "wow-cater-assets")
                        .then(el =>{

                            //console.log(el);


                            awsfunc.s3Upload(dir, img, "wow-cater-assets")
                            .then(e => {
                                //console.log(e);

                                body.img = e.Location

                                awsfunc.updateDynamoDb("cuisines", body)
                                .then(el =>{
                                    //master.admin.cuisines.push(body)

                                    for (var key in checkCuisine[0]) {
                                        if (checkCuisine[0].hasOwnProperty(key)) {
                                            checkCuisine[0][key] = body[key]
                                        }
                                    }

                                    res.json({status:"cuisine updated"});
                                })
                                .catch(err =>{
                                    console.log(err);
                                    res.json(err)
                                })
                            })
                        })
                        .catch(err =>{
                            console.log(err);
                        })
                    }
                }
                else{                    

                    if(body.img === ""){
                        body.img = "";
                    }
                    else{
                        body.img = checkCuisine[0].img
                    }

                    awsfunc.updateDynamoDb("cuisines", body)
                    .then(el =>{

                        checkCuisine[0].img = body.img;
                        checkCuisine[0].description = body.description;
                        checkCuisine[0].name = body.name;
                        res.json({status:"cuisine updated"});
                    })
                    .catch(err =>{
                        console.log(err);
                        res.json(err)
                    })

                    console.log(body);
                }
            }
            else{
                res.json({status:"cuisine not found"})
            }           

        }

        this.deleteCuisine = (master, req, res, awsfunc) =>{

            console.clear();
            let body = req.body;
            console.log(body);

            let checkCuisine = master.admin.cuisines.filter(el => el.id === body.id)

            if(checkCuisine.length){                

                if(checkCuisine[0].img === ""){

                    awsfunc.delItemsFromDynamo("cuisines", checkCuisine[0].id)
                    .then(e => {

                        master.admin.cuisines = master.admin.cuisines.filter(e => e.id != body.id)
                        res.json({ status: "cuisine Deleted" });

                    })
                    .catch(err => {
                        res.json(err)
                    })

                }
                else{

                    awsfunc.delObjS3(checkCuisine[0].img.replace("https://wow-cater-assets.s3.amazonaws.com/", "").replaceAll("%20", " "), "wow-cater-assets")
                    .then(el =>{

                        awsfunc.delItemsFromDynamo("cuisines", checkCuisine[0].id)
                        .then(e => {
    
                            master.admin.cuisines = master.admin.cuisines.filter(e => e.id != body.id)
                            res.json({ status: "cuisine Deleted" });
    
                        })
                        .catch(err => {
                            res.json(err)
                        })

                    })
                    .catch(err =>{
                        console.log(err);
                        res.json(err)
                    })

                }


            }
            else{
                res.json({status:"cuisine not found"})
            }

        }

        //==========================Cuisins==============================


        //==========================Menu types==============================


        this.addMenutype = (master, req, res, awsfunc, id) => {

            console.clear();

            let body = req.body;
            
            if(!body.id){
                body.id = id;
            }            

            if(req.files){
                let img = req.files.img;
                let dir = "menutype/" + id

                awsfunc.s3Upload(dir, img, "wow-cater-assets")
                .then(e => {
                    console.log(e);

                    body.img = e.Location

                    awsfunc.updateDynamoDb("menutype", body)
                    .then(el =>{
                        master.admin.menutype.push(body)
                        res.json({status:"menutype uploaded"});
                    })
                    .catch(err =>{
                        console.log(err);
                        res.json(err)
                    })
                })
            }
            else{
                awsfunc.updateDynamoDb("menutype", body)
                .then(el =>{
                    master.admin.menutype.push(body)
                    res.json({status:"menutype uploaded"});
                })
                .catch(err =>{
                    console.log(err);
                    res.json(err)
                })
            }          

        }

        this.updatemenutype = (master, req, res, awsfunc) =>{

            console.clear();

            let body = req.body;

            let checkmenutype = master.admin.menutype.filter(el => el.id === body.id)

            if(checkmenutype.length){

                if(req.files){

                    let img = req.files.img;
                    let dir = "menutype/" + body.id

                    console.log(img);
    
                    if(checkmenutype[0].img === ""){

                        awsfunc.s3Upload(dir, img, "wow-cater-assets")
                        .then(e => {
                            console.log(e);

                            body.img = e.Location

                            awsfunc.updateDynamoDb("menutype", body)
                            .then(el =>{
                                //master.admin.cuisines.push(body)

                                for (var key in checkmenutype[0]) {
                                    if (checkmenutype[0].hasOwnProperty(key)) {
                                        checkmenutype[0][key] = body[key]
                                    }
                                }

                                res.json({status:"menutype updated"});
                            })
                            .catch(err =>{
                                console.log(err);
                                res.json(err)
                            })
                        })    
                        
                    }
                    else{

                        awsfunc.delObjS3(checkmenutype[0].img.replace("https://wow-cater-assets.s3.amazonaws.com/", "").replaceAll("%20", " "), "wow-cater-assets")
                        .then(el =>{

                            //console.log(el);


                            awsfunc.s3Upload(dir, img, "wow-cater-assets")
                            .then(e => {
                                //console.log(e);

                                body.img = e.Location

                                awsfunc.updateDynamoDb("menutype", body)
                                .then(el =>{
                                    //master.admin.cuisines.push(body)

                                    for (var key in checkmenutype[0]) {
                                        if (checkmenutype[0].hasOwnProperty(key)) {
                                            checkmenutype[0][key] = body[key]
                                        }
                                    }

                                    res.json({status:"menutype updated"});
                                })
                                .catch(err =>{
                                    console.log(err);
                                    res.json(err)
                                })
                            })
                        })
                        .catch(err =>{
                            console.log(err);
                        })
                    }
                }
                else{

                    

                    if(body.img === ""){
                        body.img = "";
                    }
                    else{
                        body.img = checkmenutype[0].img
                    }

                    console.log(body);

                    awsfunc.updateDynamoDb("menutype", body)
                    .then(el =>{

                        checkmenutype[0].img = body.img;
                        checkmenutype[0].description = body.description;
                        checkmenutype[0].name = body.name;
                        res.json({status:"menutype updated"});
                    })
                    .catch(err =>{
                        console.log(err);
                        res.json(err)
                    })

                    
                }
            }
            else{
                res.json({status:"menutype not found"})
            }           

        }

        this.deletemenutype = (master, req, res, awsfunc) =>{

            console.clear();
            let body = req.body;
            console.log(body);

            let checkmenutype = master.admin.menutype.filter(el => el.id === body.id)

            if(checkmenutype.length){                

                if(checkmenutype[0].img === ""){

                    awsfunc.delItemsFromDynamo("menutype", checkmenutype[0].id)
                    .then(e => {

                        master.admin.menutype = master.admin.menutype.filter(e => e.id != body.id)
                        res.json({ status: "menutype Deleted" });

                    })
                    .catch(err => {
                        res.json(err)
                    })

                }
                else{

                    awsfunc.delObjS3(checkmenutype[0].img.replace("https://wow-cater-assets.s3.amazonaws.com/", "").replaceAll("%20", " "), "wow-cater-assets")
                    .then(el =>{

                        awsfunc.delItemsFromDynamo("menutype", checkmenutype[0].id)
                        .then(e => {
    
                            master.admin.menutype = master.admin.menutype.filter(e => e.id != body.id)
                            res.json({ status: "menutype Deleted" });
    
                        })
                        .catch(err => {
                            res.json(err)
                        })

                    })
                    .catch(err =>{
                        console.log(err);
                        res.json(err)
                    })

                }


            }
            else{
                res.json({status:"menutype not found"})
            }

        }

        //==========================Menu types==============================


        //==========================Miscellaneous==============================
        this.addMiscellaneous = (master, req, res, awsfunc, id) => {
            console.clear();
            let body = req.body;
            if(!body.id){
                body.id = id;
            }   
            awsfunc.updateDynamoDb("miscellaneous", body)
                .then(el =>{
                    master.admin.miscellaneous.push(body)
                    res.json({status:"miscellaneous uploaded"});
                })
                .catch(err =>{
                    console.log(err);
                    res.json(err)
                })
        }

        this.updateMiscellaneous = (master, req, res, awsfunc) =>{
            console.clear();
            let body = req.body;
            let checkMiscellaneous = master.admin.miscellaneous.filter(e => e.id === body.id)
            if(checkMiscellaneous.length){
                awsfunc.updateDynamoDb("miscellaneous", body)
                    .then(el =>{
                        res.json({status:"Miscellaneous updated"});
                    })
                    .catch(err =>{
                        console.log(err);
                        res.json(err)
                    })
            }else{
                res.json({status:"Miscellaneous not found!"})
            }
        }



    }

}

module.exports = Admin