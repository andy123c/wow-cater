const AWS = require('aws-sdk');
let awsConfig = {
    Access_key_ID:process.env.AWS_ACCESS_KEY_ID,
    Secret_access_key:process.env.AWS_SECRET_ACCESS_KEY,
    region:process.env.AWS_DEFAULT_REGION
}

AWS.config.update({
    region:awsConfig.region,
    accessKeyId:awsConfig.Access_key_ID,
    secretAccessKey:awsConfig.Secret_access_key
});

const s3 = new AWS.S3({
    Access_key_ID:process.env.AWS_ACCESS_KEY_ID,
    Secret_access_key:process.env.AWS_SECRET_ACCESS_KEY,
    region:process.env.AWS_DEFAULT_REGION
})

const dynamoClient = new AWS.DynamoDB.DocumentClient();

// tables
// --------
// company
// vendors
// menu
// type
// additional-items-menu



let company = "wow-company";
let vendor = "wow-vendor";
let admin = "wow-admin";

//select
const getDynamoData = async (table) => {
    const params = {
        TableName: table,
    };
    return await dynamoClient.scan(params).promise();
};



//single update
let updateDynamoDb = async (table,data) =>{
    const params = {
        TableName:table,
        Item:data
    }
    return await dynamoClient.put(params).promise();
}

//check update
let layerUpdate = (master, table) =>{
    master[table].forEach((e,i)=>{
        updateDynamoDb(table, e)
        .then(e =>{
            console.log(table + "updated");
        })
        .catch(err =>{
            console.log(err);
        })
    })
}



//file upload
let s3Upload = async (dir,file,bucketname) =>{
    const params = {
        Bucket:bucketname,
        Key:dir+"/"+file.name,
        Body: file.data,
        ContentType: file.mimetype
    }

    return await s3.upload(params).promise()
}



//single delete
let delItemsFromDynamo = async (table, id) =>{

    const params = {
        TableName: table,
        Key: {
            id,
        },
    }

    return await dynamoClient.delete(params).promise();
}

let delObjS3 = (dir,bucketName) =>{
    const params = {
        Bucket:bucketName,
        Key:dir
    }
    return s3.deleteObject(params).promise();
}



module.exports = {getDynamoData, updateDynamoDb, layerUpdate, s3Upload, delItemsFromDynamo, delObjS3}