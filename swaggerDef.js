// config
module.exports = {
	openapi: '3.0.1',
	info: {
		title: 'API Swagger',
		version: '1.0.0',
		description: 'API Information',
		contact: {
			name: 'Developer'
		}
	},
	servers: [{
			url : 'http://localhost:9000',
			description : 'Development server'
		},
		{
			url : 'http://34.207.72.50:9000',
			description : 'Production server'
		}
	],
	apis: ['./swagger/*.js']
};
