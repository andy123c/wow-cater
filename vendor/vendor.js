
class Vendors {
    constructor() {


        this.getAll = () =>{
            console.log("vendor all");
        }


        this.registerVendor = (master,res,awsfunc, body) =>{
            awsfunc.updateDynamoDb("vendors", body)
            .then(e =>{
                master.vendors.push(body);
                res.json({status:"Registered", id:body.id});
            })
            .catch(err =>{
                res.json(err)
            })
        }

        this.updateVendor = (master,res,awsfunc, body) =>{
            awsfunc.updateDynamoDb("vendors", body)
            .then(e =>{
                let checkvendors = master.vendors.filter(e => e.id === body.id)

                if(checkvendors.length){

                    for (var key in checkvendors[0]) {
                        if (checkvendors[0].hasOwnProperty(key)) {
                            checkvendors[0][key] = body[key]
                        }
                    }

                    res.json({status:"Updated"});
                }
                else{
                    res.json({status:"something wrong"});
                }                

            })
            .catch(err =>{
                res.json(err)
            })
        }

        this.deleteVendor = (master,res,awsfunc, body) =>{


            // awsfunc.delItemsFromDynamo("vendors", body.id)
            // .then(e =>{
                
            //     master.vendors = master.vendors.filter(e => e.id != body.id)
            //     res.json({status:"vendor Deleted"});

            // })
            // .catch(err =>{
            //     res.json(err)
            // })

            let checkvendors = master.vendors.filter(e => e.id === body.id)
            //let checkvendors = master.vendors.filter(e => e.id === body.id)

            if(checkvendors.length){

                if (checkvendors[0].logo === "") {
                    awsfunc.delItemsFromDynamo("vendors", checkvendors[0].id)
                        .then(e => {
    
                            master.vendors = master.vendors.filter(e => e.id != body.id)
                            res.json({ status: "vendors Deleted" });
    
                        })
                        .catch(err => {
                            res.json(err)
                        })
                }
                else {
                    awsfunc.delObjS3(checkvendors[0].logo.replace("https://wow-cater-assets.s3.amazonaws.com/", "").replaceAll("%20", " "), "wow-cater-assets")
                        .then(e => {
                            awsfunc.delItemsFromDynamo("vendors", body.id)
                                .then(e => {
                                    master.vendors = master.vendors.filter(e => e.id != body.id)
                                    res.json({ status: "vendors Deleted" });
    
                                })
                                .catch(err => {
                                    res.json(err)
                                })
                        })
                }

            }   
            else{
                res.json({ status: "vendors not found" });
            }


        }

        this.uploadLogo = (master,res,awsfunc, body, file) =>{

            let dir = "vendors/"+body.id+"/logo";

            let checkvendors = master.vendors.filter(e => e.id === body.id)
                
            if(checkvendors.length){
                
                if(checkvendors[0].logo != ""){
                                            
                    awsfunc.delObjS3(checkvendors[0].logo.replace("https://wow-cater-assets.s3.amazonaws.com/","").replaceAll("%20", " "), "wow-cater-assets")
                    .then(el => {
                        console.log(el);

                        awsfunc.s3Upload(dir, file.img, "wow-cater-assets")
                        .then(e =>{
                            console.log(e);
                            checkvendors[0].logo = e.Location;

                            awsfunc.updateDynamoDb("vendors", checkvendors[0])
                            .then(el =>{
                                console.log(el);
                                res.json({status:"logo uploaded"});
                            })
                            .catch(err =>{
                                console.log(err);
                                res.json(err)
                            })
                        })
                        .catch(err =>{
                            res.json(err)
                        })
                    })
                    .catch(err =>{
                        res.json(err)
                    })

                }
                else{

                    awsfunc.s3Upload(dir, file.img, "wow-cater-assets")
                    .then(e =>{
                        console.log(e);
                        checkvendors[0].logo = e.Location;

                        awsfunc.updateDynamoDb("vendors", checkvendors[0])
                        .then(el =>{
                            console.log(el);
                            res.json({status:"logo uploaded"});
                        })
                        .catch(err =>{
                            console.log(err);
                            res.json(err)
                        })
                    })
                    .catch(err =>{
                        res.json(err)
                    })                        
                }
            }
            else{
                res.json({status:"no entry found!"});
            }
        }


        this.uploadmenu = (master,res,awsfunc, body, file) =>{

            let dir = "menu/"+body.id+"/img";

            awsfunc.s3Upload(dir, file.img, "wow-cater-assets")
            .then(e =>{
                console.log(e);
                body.img = e.Location;

                awsfunc.updateDynamoDb("menu", body)
                .then(el =>{
                    console.log(el);
                    master.admin.menus.push(body)
                    res.json({status:"menu uploaded"});                    
                })
                .catch(err =>{
                    console.log(err);
                    res.json(err)
                })
            })
            .catch(err =>{
                res.json(err)
            })  

        }


        // additionalMenus: ""
        // cuisines: "8sb52u4l59toa0o"
        // description: "123"
        // image: File {name: 'indian.jpg', lastModified: 1657125318165, lastModifiedDate: Wed Jul 06 2022 22:05:18 GMT+0530 (India Standard Time), webkitRelativePath: '', size: 43710, …}
        // menutype: "8sb52u4l59u7vmv"
        // name: "123"

        this.updateMenu = (master,req,res,awsfunc) =>{


            console.clear();

            let body = req.body;
           
            let dir = "menu/"+body.id+"/img";

            let checkMenu = master.admin.menus.filter(el => el.id === body.id)

            if(checkMenu.length){

                if(req.files){


                    awsfunc.delObjS3(checkMenu[0].image.replace("https://wow-cater-assets.s3.amazonaws.com/", "").replaceAll("%20", " "), "wow-cater-assets")
                    .then(e => {


                        awsfunc.s3Upload(dir, req.files.img, "wow-cater-assets")
                        .then(e =>{
                            console.log(e);
                            body.img = e.Location;

                            awsfunc.updateDynamoDb("menu", body)
                            .then(el =>{
                                console.log(el);
                                for (var key in checkMenu[0]) {
                                    if (checkMenu[0].hasOwnProperty(key)) {
                                        checkMenu[0][key] = body[key]
                                    }
                                }
                                res.json({status:"menu updated"});                    
                            })
                            .catch(err =>{
                                console.log(err);
                                res.json(err)
                            })
                        })
                        .catch(err =>{
                            res.json(err)
                        }) 


                    })

                }
                else{

                    awsfunc.updateDynamoDb("menu", body)
                    .then(el =>{

                        for (var key in checkMenu[0]) {
                            if (checkMenu[0].hasOwnProperty(key)) {
                                checkMenu[0][key] = body[key]
                            }
                        }

                        res.json({status:"menu updated"});                    
                    })
                    .catch(err =>{
                        console.log(err);
                        res.json(err)
                    })

                }

            }
            else{
                res.json({status:"menu not found!"})
            }

        }




        this.createMenuList = (master,req, res,awsfunc, id) =>{

            console.clear();

            // {
            //     id:"",
            //     name:"",
            //     items:[],
            //     vendorId:""
            // }

            let body = req.body;
            body.id = id

            let checkvendors = master.vendors.filter(e => e.id === body.vendorId)

            if(checkvendors.length){

                
                awsfunc.updateDynamoDb("vendors", body)
                .then(e =>{
                    checkvendors[0].menus.push(body)
                    res.json({status:"Vendor's menu list was enlisted in their profile!"})
                })
                .catch(err =>{
                    res.json(err)
                })               

            }
            else{
                res.json({status:"Vendor not found!"})
            }
        }


        this.deleteMenuList = (master, req,res,awsfunc) =>{

            console.clear();

            // {
            //     id:"",
            //     name:"",
            //     items:[],
            //     vendorId:""
            // }

            let body = req.body

            let checkvendors = master.vendors.filter(e => e.id === body.vendorId)

            if(checkvendors.length){


                checkvendors[0].menus = checkvendors[0].menus.filter(el => el.id != body.id)

                awsfunc.updateDynamoDb("vendors", checkvendors[0])
                .then(e =>{
                    res.json({status:"Vendor's menu list was enlisted in their profile!"})
                })
                .catch(err =>{
                    res.json(err)
                })               

            }
            else{
                res.json({status:"Vendor not found!"})
            }
        }


        this.editMenuList = (master,req,res,awsfunc) =>{

            console.clear();

            // {
            //     id:"",
            //     name:"",
            //     items:[],
            //     vendorId:""
            // }

            let body = req.body

            let checkvendors = master.vendors.filter(e => e.id === body.vendorId)

            if(checkvendors.length){


                let checkMenuList = checkvendors[0].menus.filter(el => el.id === body.id)


                if(checkMenuList.lenght){
                    checkMenuList[0].items = body.items;

                    awsfunc.updateDynamoDb("vendors", checkvendors[0])
                    .then(e =>{
                        res.json({status:"Vendor's menu list edited!"})
                    })
                    .catch(err =>{
                        res.json(err)
                    })

                }
                else{
                    res.json({status:"Menulist not found!"})
                }                           

            }
            else{
                res.json({status:"Vendor not found!"})
            }
        }

        this.vendorProfile = (master,req,res,awsfunc) =>{
            let body = req.body
            let checkvendors = master.vendors.filter(e => e.id === body.id)
            if(checkvendors.length){
                let vendorProfile = checkvendors[0]
                delete vendorProfile["pw"]
                res.json({status:"Vendor profile!",data:vendorProfile})
            }else{
                res.json({status:"Vendor not found!"})
            }
        }

        //==========================Meals==============================

        this.addMeal = (master, req, res, awsfunc, id) => {
            console.clear();
            let body = req.body;
            if(!body.id){
                body.id = id;
            }   
            awsfunc.updateDynamoDb("meals", body)
                .then(el =>{
                    master.admin.meals.push(body)
                    res.json({status:"meals uploaded"});
                })
                .catch(err =>{
                    console.log(err);
                    res.json(err)
                })
        }

        this.editMeal = (master, req, res, awsfunc) =>{
            console.clear();
            let body = req.body;
            let checkMeal = master.admin.meals.filter(e => e.id === body.id)
            if(checkMeal.length){
                res.json({status:"meals found!",data:checkMeal});
            }else{
                res.json({status:"Meal not found!"})
            }
        }

        this.updateMeal = (master, req, res, awsfunc) =>{
            console.clear();
            let body = req.body;
            let checkMeal = master.admin.meals.filter(e => e.id === body.id)
            if(checkMeal.length){
                awsfunc.updateDynamoDb("meals", body)
                    .then(el =>{
                        res.json({status:"Meal updated"});
                    })
                    .catch(err =>{
                        console.log(err);
                        res.json(err)
                    })
            }else{
                res.json({status:"Meal not found!"})
            }
        }

        this.deleteMeal = (master, req, res, awsfunc) =>{
            console.clear();
            let body = req.body;
            let checkMeal = master.admin.meals.filter(e => e.id === body.id)
            if(checkMeal.length){
                awsfunc.delItemsFromDynamo("meals", checkMeal[0].id)
                    .then(e => {

                        master.admin.meals = master.admin.meals.filter(e => e.id != body.id)
                        res.json({ status: "Meal Deleted" });

                    })
                    .catch(err => {
                        res.json(err)
                    })
            }else{
                res.json({status:"Meal not found!"})
            }
        }

        this.orderList = (master, req, res, awsfunc) =>{
            console.clear();
            let body = req.body;

            let checkOrderList = master.admin.orders.filter(e => e.vendor_id === body.vendor_id)
            if(checkOrderList.length){
                res.json({status:"Order listed!",data:checkOrderList})
            }else{
                res.json({status:"Order not found!"})
            }
        }

        this.orderDetails = (master, req, res, awsfunc) =>{
            console.clear();
            let body = req.body;
            let checkOrder = master.admin.orders.filter(e => e.id === body.order_id && e.vendor_id === body.vendor_id)
            if(checkOrder.length){
                res.json({status:"Order found!",data : checkOrder[0]})
            }else{
                res.json({status:"Order not found!"})
            }
        }


    }


    
}

module.exports = Vendors;